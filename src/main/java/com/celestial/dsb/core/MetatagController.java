package com.celestial.dsb.core;

import java.util.ArrayList;

import com.celestial.dsb.core.db.MetatagDAO;

public class MetatagController {
	 private final MetatagDAO itsMetatagDao = new MetatagDAO();
	 
	 public ArrayList<String> getAllTags(){
		 return itsMetatagDao.getAllTags();
	 }
	 
	 public ArrayList<String> getTagsForLink( int bookmarkId ){
		 return itsMetatagDao.getTagsForLink(bookmarkId);
	 }
}
