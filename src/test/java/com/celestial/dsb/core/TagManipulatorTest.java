/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.dsb.core;

import static org.hamcrest.CoreMatchers.is;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Selvyn
 */
public class TagManipulatorTest
{
    private TagManipulator  tagManipulator;
    
    public TagManipulatorTest()
    {
    }
    
    @Before
    public void setUp()
    {
        tagManipulator = new TagManipulator();
    }

    @Test
    public void separate_out_empty_tag_list()
    {
        // setup
        TagManipulator tagManipulator = new TagManipulator();
        String  input = "shopping";
        int expected = 1;
        String[] expected_tags = {"shopping"};

        // execute
        String[] result = tagManipulator.parseString(input, ",\\ *");

        // test
        assertThat(result.length, is(expected) );
        assertArrayEquals(expected_tags, result);
    }

    //++ insert code here
    @Test
    public void seperate_out_tag_list_of_many () {
        String input = "cars, motorcycles, buses";
        int expected = 3;
        String[] expected_tags = {"cars", "motorcycles", "buses"};
        
        // execute
        String [] result = tagManipulator.parseString(input, ",\\ *");
        
        //test
        assertThat(result.length, is(expected) );
        assertArrayEquals(expected_tags, result);
        
    }
    
    //++ insert code here
}
